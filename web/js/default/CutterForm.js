class CutterForm {
    constructor()
    {
        this.form = document.getElementById('cutter_form');
        this.link_input = document.getElementById('cf_link_input');
        this.expire_date_input = document.getElementById('cf_expire_date_input');
        this.saved_link_input = document.getElementById('cf_saved_link_input');
        this.saved_link_copy_btn = document.getElementById('cf_saved_link_copy_btn');
        this.inputs_block = document.getElementById('cutter_form_inputs_block');
        this.success_block = document.getElementById('cf_success_block');
        this.more_links_btn = document.getElementById('cf_more_links_btn');
        this.submit_btn = document.getElementById('cf_submit_btn');
    }

    initFormWidgets()
    {
        let self = this;

        // Init expire date input
        $(self.expire_date_input).datetimepicker({
            format:'d.m.Y / H:i',
            timepicker: true,
        });

        // Copy link button
        $(self.saved_link_copy_btn).off('click').on('click', function (e) {
            e.preventDefault();
            let $temp = $("<input>");
            $('body').append($temp);
            $temp.val($(self.saved_link_input).val()).select();
            document.execCommand("copy");
            $temp.remove();
            alert('Url copied to the clipboard!');
        });

        // More links button
        $(self.more_links_btn).off('click').on('click', function (e) {
            e.preventDefault();
            $(self.success_block).hide();
            $(self.inputs_block).stop().fadeIn(300);
        });

        // Init submit button click
        $(self.submit_btn).off('click').on('click', function (e) {
            e.preventDefault();

            if (!$(self.link_input).val().match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/)) {
                alert('Link not correct!');
                return false;
            }
            if (!$(self.expire_date_input).val().match(/\d{2}\.\d{2}.\d{4} \/ \d{2}:\d{2}/)) {
                alert('Date not correct!');
                return false;
            }

            $(self.submit_btn).prop('disabled', true);

            let form_data = new FormData(self.form);

            $.ajax({
                url: '/default/upload-create-link-form',
                method: 'POST',
                data: form_data,
                processData: false,
                contentType: false,
                success: function (response) {
                    if (typeof response.link !== 'undefined' && response.link) {
                        let new_link = response.link;

                        $(self.inputs_block).hide();
                        $(self.success_block).stop().fadeIn(300);

                        $(self.saved_link_input).val(window.location.protocol + '//' + window.location.host + '/go/' + new_link.link_hash);
                    } else {
                        alert('Server error!');
                    }
                },
                error: function () {
                    alert('Server error!');
                },
                complete: function () {
                    $(self.link_input).val('');
                    $(self.submit_btn).prop('disabled', false);
                }
            });
        });
    }

    run()
    {
        let self = this;

        self.initFormWidgets();
    }
}