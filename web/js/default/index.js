window.di_page_functions = {
    initCutterForm: function () {
        window.cutter_form = new CutterForm();
        window.cutter_form.run();
    }
};

$(document).ready(function () {
    window.di_page_functions.initCutterForm();
});