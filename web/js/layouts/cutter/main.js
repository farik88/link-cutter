window.cutter_layout_functions = {
    setGlobalParams: function () {
        jQuery.datetimepicker.setLocale('en');
    }
};

$(document).ready(function () {
    window.cutter_layout_functions.setGlobalParams();
});