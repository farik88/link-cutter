<?php
namespace app\controllers;

use app\models\SavedLinks;
use Yii;
use app\controllers\base\BaseController;
use yii\base\Exception;
use yii\web\Response;

class DefaultController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLinkExpired()
    {
        return $this->render('link-expired');
    }

    public function actionStats()
    {
        $all_links = SavedLinks::find()->orderBy('created_at DESC')->all();

        return $this->render('stats', [
            'all_links' => $all_links
        ]);
    }

    public function actionUploadCreateLinkForm()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $response = [];
        Yii::$app->response->format = Response::FORMAT_JSON;
        $posted_link = Yii::$app->request->post('Link');

        if (!isset($posted_link['url']) || empty($posted_link['url'])) {
            throw new Exception(500, 'Invalid link url!');
        }
        if (!isset($posted_link['datetime']) || empty($posted_link['datetime'])) {
            throw new Exception(500, 'Invalid expire date!');
        }

        $new_link = new SavedLinks();
        $new_link->link_url = trim($posted_link['url']);
        $new_link->link_hash = SavedLinks::generateNewHash();

        // Get datetime timestamp
        $dt = \DateTime::createFromFormat('d.m.Y / H:i', $posted_link['datetime']);
        $new_link->expire_timestamp = $dt->getTimestamp();

        // Save new link and return it
        $new_link->save();
        $response['link'] = $new_link->toArray();

        return $response;
    }
}