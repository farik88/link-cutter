<?php
namespace app\controllers;

use app\models\SavedLinks;
use Yii;
use app\controllers\base\BaseController;

class GoController extends BaseController
{
    public function actionIndex()
    {
        $link_hash = Yii::$app->request->get('link_hash');
        $now_time = new \DateTime('NOW');

        $saved_link = SavedLinks::find()
            ->where(['link_hash' => $link_hash])
            ->one();

        if (!$saved_link) {
            $this->redirect('/default/link-expired');
        } else if ($saved_link->expire_timestamp <= $now_time->getTimestamp()) {
            $this->redirect('/default/link-expired');
        } else {
            $saved_link->visits++;
            $saved_link->save();
            $this->redirect($saved_link->link_url);
        }
    }
}