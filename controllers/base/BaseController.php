<?php
namespace app\controllers\base;

use yii\web\Controller;

class BaseController extends Controller
{
    public function beforeAction($action)
    {
        $this->layout = '@app/views/layouts/cutter/main.php';

        return parent::beforeAction($action);
    }
}