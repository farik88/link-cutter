<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%saved_links}}`.
 */
class m201210_203213_create_saved_links_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%saved_links}}', [
            'id' => $this->primaryKey(),
            'link_url' => $this->string(2000)->notNull(),
            'link_hash' => $this->string(12)->unique()->notNull(),
            'expire_timestamp' => $this->bigInteger(10)->notNull(),
            'visits' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->bigInteger(10),
            'updated_at' => $this->bigInteger(10),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%saved_links}}');
    }
}
