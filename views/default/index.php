<?php
use app\assets\CutterAsset;

$this->title = 'Link cutter';

$this->registerCssFile('@web/css/default/index.css', ['depends' => CutterAsset::class]);
$this->registerJsFile('@web/js/default/CutterForm.js', ['depends' => CutterAsset::class]);
$this->registerJsFile('@web/js/default/index.js', ['depends' => CutterAsset::class]);

$now_dt = new DateTime('NOW');
?>

<section id="cutter_block">
    <div class="inner">
        <form id="cutter_form" method="POST">
            <div id="cutter_form_inputs_block">
                <h2 class="form-heading-title">We can make your link shorter!</h2>
                <div class="form-group">
                    <label for="cf_link_input">Url:</label>
                    <input id="cf_link_input" type="text" name="Link[url]" class="cutter-form-input" placeholder="Put your link here..." required>
                </div>
                <div class="form-group">
                    <label for="cf_expire_date_input">Expire date:</label>
                    <input id="cf_expire_date_input" name="Link[datetime]" class="cutter-form-input" type="text" value="<?= $now_dt->format('d.m.Y / H:i') ?>">
                </div>
                <div class="form-group">
                    <button id="cf_submit_btn" class="cutter-form-button">GENERATE</button>
                </div>
            </div>
            <div id="cf_success_block">
                <h2 class="form-success-title">Here is your short link</h2>
                <div>
                    <a id="cf_saved_link" class="text-link" href="#" target="_blank"></a>
                </div>
                <div class="saved-link-input-wrap">
                    <input id="cf_saved_link_input" class="cutter-form-input" type="text">
                    <div id="cf_saved_link_copy_btn">
                        <svg class="svg-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 469.333 469.333" style="enable-background:new 0 0 469.333 469.333;" xml:space="preserve"><g><g><g><path d="M330.667,0h-256C51.093,0,32,19.093,32,42.667v298.667h42.667V42.667h256V0z"/><path d="M394.667,85.333H160c-23.573,0-42.667,19.093-42.667,42.667v298.667c0,23.573,19.093,42.667,42.667,42.667h234.667c23.573,0,42.667-19.093,42.667-42.667V128C437.333,104.427,418.24,85.333,394.667,85.333z M394.667,426.667H160V128h234.667V426.667z"/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
                    </div>
                </div>
                <div class="buttons-wrap">
                    <button id="cf_more_links_btn" class="cutter-form-button">MORE LINKS</button>
                </div>
            </div>
        </form>
    </div>
</section>
