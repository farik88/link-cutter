<?php
/**
 * @var $all_links array
 */

use app\assets\CutterAsset;

$this->title = 'Links visits stats';

$this->registerCssFile('@web/css/default/stats.css', ['depends' => CutterAsset::class]);
$this->registerJsFile('@web/js/default/stats.js', ['depends' => CutterAsset::class]);

$now_dt = new DateTime('NOW');
?>

<div id="stats_block">
    <h2 class="stats-heading-title">
        All links visits
    </h2>
    <?php if (!empty($all_links)) { ?>
        <div class="stats-table-wrap x-scroll y-scroll">
            <table class="stats-table">
                <thead>
                    <tr>
                        <th>Hash</th>
                        <th>Link</th>
                        <th>Visits</th>
                        <th>Expire</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($all_links as $one_link_key => $one_link) { ?>
                    <tr>
                        <td>
                            <?= $one_link->link_hash ?>
                        </td>
                        <td>
                            <a class="full-link" href="<?= $one_link->link_url ?>" target="_blank" title="<?= $one_link->link_url ?>"><?= $one_link->link_url ?></a>
                        </td>
                        <td>
                            <?= $one_link->visits ?>
                        </td>
                        <td>
                            <?php
                                $expire_dt = new DateTime();
                                $expire_dt->setTimestamp($one_link->expire_timestamp);
                            ?>
                            <span class="expire-time-info <?= ($now_dt->getTimestamp() >= $one_link->expire_timestamp) ? ' expired' : ' not-expired' ?>">
                                <?= $expire_dt->format('d.m.Y / H:i') ?>
                            </span>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } else { ?>
        <span class="no-stats-message">
            No links seem to have been created yet...
        </span>
    <?php } ?>
</div>