<?php
use app\assets\CutterAsset;

$this->title = 'Opps...';

$this->registerCssFile('@web/css/default/link-expired.css', ['depends' => CutterAsset::class]);
?>

<div id="le_main">
    <div class="inner">
        <div class="heading-title-wrap">
            <h1 class="heading-title-text">Opps... Looks like the link has been removed or has expired :(</h1>
        </div>
        <div class="back-button-wrap">
            <a href="/" class="cutter-form-button">TO MAIN PAGE</a>
        </div>
    </div>
</div>
