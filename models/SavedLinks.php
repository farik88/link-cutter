<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "saved_links".
 *
 * @property int $id
 * @property string|null $link_url
 * @property string|null $link_hash
 * @property int|null $expire_timestamp
 * @property int $visits
 * @property int|null $created_at
 * @property int|null $updated_at
 */
class SavedLinks extends \yii\db\ActiveRecord
{
    const HASH_LENGTH = 12;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'saved_links';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => time(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link_url', 'link_hash', 'expire_timestamp'], 'required'],
            [['expire_timestamp', 'visits', 'created_at', 'updated_at'], 'integer'],
            [['link_url'], 'string', 'max' => 2000],
            [['link_hash'], 'string', 'max' => self::HASH_LENGTH],
            [['link_hash'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link_url' => 'Link Url',
            'link_hash' => 'Link Hash',
            'expire_timestamp' => 'Expire Timestamp',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function generateNewHash()
    {
        $random_hash = null;
        $is_new_hash = false;

        while (!$is_new_hash) {
            $random_hash = Yii::$app->security->generateRandomString(self::HASH_LENGTH);
            $link_with_hash = SavedLinks::find()->where(['link_hash' => $random_hash])->one();

            if (!$link_with_hash) {
                $is_new_hash = true;
            }
        }

        return $random_hash;
    }
}
