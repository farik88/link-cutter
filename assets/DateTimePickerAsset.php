<?php

namespace app\assets;

use yii\web\AssetBundle;

class DateTimePickerAsset extends AssetBundle
{
    public $sourcePath = '@bower/datetimepicker';
    public $css = [
        'jquery.datetimepicker.css'
    ];
    public $js = [
        'build/jquery.datetimepicker.full.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}