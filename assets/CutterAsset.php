<?php

namespace app\assets;

use yii\web\AssetBundle;

class CutterAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/layouts/cutter/main.css'
    ];
    public $js = [
        'js/layouts/cutter/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'app\assets\DateTimePickerAsset',
    ];
}
